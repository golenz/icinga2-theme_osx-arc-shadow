## Icinga2 _OSX-Arc-Shadow_ Theme 

This theme is not perfect but can be great to edit yourself

put _"osx-arc-shadow.less"_ in `/usr/share/icingaweb2/public/css/themes`

![alt text](https://gitlab.com/golenz/icinga2-theme_osx-arc-shadow/raw/master/preview/icingaweb2-UI.png)
![alt text](https://gitlab.com/golenz/icinga2-theme_osx-arc-shadow/raw/master/preview/icingaweb2-login.png)
